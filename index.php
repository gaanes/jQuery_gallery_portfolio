<!DOCTYPE html>
<html>
<head>
	<title>My PhotoGraphy Portfolio</title>
	<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="js/portfolio.js"></script>
	<script type="text/javascript" src="js/filter.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.8.2.custom.min.js"></script>
	<link rel="stylesheet" type="text/css" href="css/portfolio.css">
</head>
<body>
<div id="header">
	<div id="menu">
		<h2>
			Photography Portfolio
		</h2>
		<input id="search" style="float:right" type="text" placeholder="search..." >
	</div>
</div>
	<div id="overlay"></div>
	<div id="frame">
	<table id="frame-table">
		<tr>
			<td id="left">
				<img src="images/left.png" alt="left"/>
			</td>
			<td id="right">
				<img src="images/right.png" alt="right"/>
			</td>
		</tr>
	</table>
		<img id="main" src="" alt=""/>
		<div id="description">
			<p></p>
		</div>
	</div>
<div id="wrapper">
<ul id="filter">
	<li class="active">all</li>
	<li>nature</li>
	<li>animal</li>
	<li>architecture</li>
</ul>
	<ul id="portfolio">
		<?php include_once("list.html") ?>
	</ul>
</div>
</body>
</html>